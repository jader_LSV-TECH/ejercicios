"""import http.server
import socketserver
PORT = 9090

Handler = http.server.SimpleHTTPRequestHandler
with socketserver.TCPServer(("", PORT), Handler) as mi_servidor:
    print("Servidor en el puerto 9090")
    mi_servidor.serve_forever()"""

##################################################################################

#Lo primero que hacemos es importar las librerias necesarias
from http.server import HTTPServer, BaseHTTPRequestHandler
from os import curdir, sep
"""
 Creamos una clase a la cual yo llamare "HttpHandler" que heredara 
 los metodos de la clase BaseHTTPRequestHandler.
"""
class HttpHandler(BaseHTTPRequestHandler):
    #Usaremos el metodo do_get, el cual es un metodo de BaseHTTPRquestsHandler.
    #Debemos recprdar que podemos hacer 2 tipos de peticiones, POST y GET
    #Usaremos GET

    def do_GET(self):

        #En este metodo definiremos las caractersticas de nuestro servidor HTTP.
        #Primero, enviamos una respuesta 200.
        #Eso lo logramos con el metodo send_response, psandole como argumento una respuesta
        #self.send_response(200)

        #Luego, enviamos las cabezeras, yo enviare solo el tipo del contenido
        #Usamos el metodo send_header, pasandole como argumento la informacion deseada
        #self.send_header('Content-type', 'text/HTML; charset=utf-8')
        #Con el metodo end_headers, terminamos de colocar las cabezeras del servidor
        #self.end_headers()

        #Creamos una variable que contendra nuestro mensaje.
        #Fijense que estoy usando etiquetas HTML, ya que lo defini el los headers
        #message = "<h1>Hola amigos de Mi Diario Python :D</h1>".encode()
        #Ahora, escribimos la respuesta en el cuerpo de la pagina
        #self.wfile.write(message)

        if self.path == "/":
            self.path = "/index.html"

        try:
            # Check the file extension required and
            # set the right mime type

            sendReply = False
            if self.path.endswith(".html"):
                mimetype = 'text/html'
                sendReply = True
            if self.path.endswith(".jpg"):
                mimetype = 'image/jpg'
                sendReply = True
            if self.path.endswith(".gif"):
                mimetype = 'image/gif'
                sendReply = True
            if self.path.endswith(".js"):
                mimetype = 'application/javascript'
                sendReply = True
            if self.path.endswith(".css"):
                mimetype = 'text/css'
                sendReply = True

            if sendReply == True:
                # Open the static file requested and send it
                f = open(curdir + sep + self.path, 'br')
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
            return
        except IOError:
            #sendReply = False
            """
            if self.path.endswith(".html"):
                mimetype = 'text/html'
                sendReply = True
            """

            #if sendReply == True:
            # Open the static file requested and send it
            f = open(curdir + sep + 'error.html', 'br')
            self.send_response(200)
            #self.send_header('Content-type', mimetype)
            self.end_headers()
            self.wfile.write(f.read())
            #f.close()

            # self.send_error(404, 'File Not Found: %s' % self.path)
            # self.send_response(200)
            # self.end_headers()
            # message = "<h1>Esta Pagina no esta .....</h1>".encode()
            # self.wfile.write(message)

if __name__ == "__main__":
	server_address = ('', 8000)
	httpd = HTTPServer(server_address, HttpHandler)
	httpd.serve_forever()

"""
    def do_GET(self):
        if self.path == "/":
            self.path = "/index_example2.html"

        try:
            # Check the file extension required and
            # set the right mime type

            sendReply = False
            if self.path.endswith(".html"):
                mimetype = 'text/html'
                sendReply = True
            if self.path.endswith(".jpg"):
                mimetype = 'image/jpg'
                sendReply = True
            if self.path.endswith(".gif"):
                mimetype = 'image/gif'
                sendReply = True
            if self.path.endswith(".js"):
                mimetype = 'application/javascript'
                sendReply = True
            if self.path.endswith(".css"):
                mimetype = 'text/css'
                sendReply = True

            if sendReply == True:
                # Open the static file requested and send it
                f = open(curdir + sep + self.path)
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
            return


        except IOError:
            self.send_error(404, 'File Not Found: %s' % self.path)

        try:
            # Create a web server and define the handler to manage the
            # incoming request
            server = HTTPServer(('', PORT_NUMBER), myHandler)
            print
            'Started httpserver on port ', PORT_NUMBER

            # Wait forever for incoming htto requests
            server.serve_forever()

        except KeyboardInterrupt:
            print('received, shutting down the web server')
            server.socket.close()
"""

