import requests

def funcion1():
    lista=[]
    url = "https://jsonplaceholder.typicode.com/posts"
    response = requests.get(url)
    if response.status_code == 200:
        results = response.json()
        for result in results[0:10]:
            lista.append(result['title'])
    else:
        print ("Error code %s" % response.status_code)

    return lista

def fotos():
    fotos=[]
    url="https://jsonplaceholder.typicode.com/photos/"
    response = requests.get(url)
    if response.status_code == 200:
        results = response.json()
        for result in results[0:8]:
            #fotos.append(result['title'])
            fotos.append(result)
    else:
        print ("Error code %s" % response.status_code)
    return fotos

def comentario():
    comentario=[]
    url="https://jsonplaceholder.typicode.com/comments"
    response = requests.get(url)
    if response.status_code == 200:
        results = response.json()
        for result in results[0:8]:
            #fotos.append(result['title'])
            comentario.append(result['body'])
    else:
        print ("Error code %s" % response.status_code)
    return comentario



######################### Consumiendo la API ###################################
mis_tittulos = ''
for l in funcion1():
    mis_tittulos +='<p>'+l+'</p>'

comen=''
for y in comentario():
    comen=y

mis_fotos='<div class="container row">'
for x in fotos():
    comen = ''
    for y in comentario():
        comen = y

    #print(x['albumId'])
    mis_fotos+='<div class="col-sm-12 col-md-4 col-lg-3">' \
               '<div class="card" style="width: 18rem;">' \
                    '<img src="..." class="card-img-top" alt="...">' \
                    '<div class="card-body"> ' \
                        '<h5 class="card-title">'+x.get('title')+'</h5>'\
                        '<p>'+comen+'</p>'\
                        '</p> <a href="#" class="btn btn-primary">'+str(x.get('id'))+'</a> ' \
                    '</div>' \
                '</div>' \
            '</div>'\

mis_fotos=mis_fotos+'</div>'



file =open('pagina.html','w')

file.write('<html>')
file.write('<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">')
file.write('<h1 align="center">LSV - TEAM </h1>')
file.write('')

#file.write(mis_tittulos)
file.write(mis_fotos)

file.write('<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>')
file.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>')
file.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>')
file.write('</html>')

file.close()