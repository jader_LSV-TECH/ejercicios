"""def primos(n):
    numeros=[]
    if n < 1:
        return "Ni primo, ni compuesto"
    for i in range(2,n):
        if n % i== 0:
            numeros.append(i)
    return numeros

#print(primos(20))
"""

def primo(num):
    if num < 2:                 #Esta condicion no se toma en cuenta
        return False
    for i in range(2, num):
        if num % i == 0:        #si el resto es diferente de un entro sera tomado encuenta de lo contrario pailas
            return False
    return True                 #en caso de no cumplir la condicion es Verdadera
"""
def imprime_num(n):
    cont=0                      #contador para las veces que se imprima
    for x in range(n+1):
        if primo(x)==True:      # Llamada a la funcion que determina si un numero es o no primo
            cont += 1           # cuenta el ciclo cada ves que la condicion se cumple
            print(x,end=' ')    # Imprime la variable ademas de darle formato horizontal
            if cont==10:        # cuando el contador llega a 10 se pausa la impresion
                break


print(imprime_num(100))"""

def isPrime(n):
    if n < 2: return "Neither prime, nor composite"
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def NumerosPrimos(n):
    ContaPrim = 0
    prim = 1

    while ContaPrim < n:
        prim += 1
        if primo(prim):
            ContaPrim += 1
    return prim

print(NumerosPrimos(1001))