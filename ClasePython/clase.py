#b="Hola Lia Maria"
"""c="Hola Betzabe"

for x in range(1,9):
    a="Hola Jader"
    print(a)

def casa():
    b = "Hola Carmen Diaz"
    return a

def mapa():
    return b


print("reutilizando a: ",a)
print("reutilizando b: ",casa())
print("reutilizando b: ",mapa())"""
######################################################

#ejemplo Extend

#my_list=[4,3,7,6,23,4,67,]
#my_list=["a","c","d","y","w","n",2,5,7,1]
#my_list=["manzana","peras","monzones","peras","uvas"]
cadena="hola, como estas te ves igual, hace tiempo, que ya no, nose de. ti"

#my_list.extend([2,5])              #----> extiende el vector con valores asignados
#my_list.append([5,7])              #----> introduce valores a la matriz
#my_list.remove(1)                  #----> elimina el valor del indice
#my_list.pop(7)                     #----> quita el elemento segun indice
#my_list.clear()                    #----> Borra todo el array
#my_list.count(3)                   #----> Me devuelve el elemento repetido en una matriz
#my_list.insert(3,"casa")           #----> Inserta un elemento en la posicion dada
#my_list[7]=777                     #----> Remplaza el Valor del index por el asignado
#my_list.sort()                     #----> Ordena la Lista de menor a mayor
#cadena.split()                     #----> separa las palabras y las devuelve como vector (Metodo de Cadena)
#print(cadena.split())
########################################### DICCIONARIOS ###############################################################

#DICCIONARIOS#
key:'value' # se  puede poner valor que sea
diccionario={'Nombre':'Jonathan', 'a':'Maritza'}
dic={'name':'Jader','lastname':'Yepez'}
dic1={'name':'Eloy','lastname':'Diaz','address':'Barranquilla','phone':'097654','house':'casa mia'}

"""
#print(dic)
#print(dic1['lastname'])

for key in dic1:
    print(key)"""

#TUPLAS las tuplas se declaran dentro del DICT por fuera no se puede
#print (dict(name='jonathan',lastname='martinez'))
#print(dict(zip('abcd',[1,2,3,4]))) #---> Zip desempaquta objetos iterables (solo funciona como en el ejemplo)

#ITEMS() DEVUELVE LAS LLAVES Y SUS VALORES
#print(dic.items())

#KEYS DEVUELVE LAS LLAVES DEL DICCIONARIO
#print(dic.keys())

#VALUES (Devuelve los valores de las llaves)
#print(dic.values())

# claer
#print(dic.clear())

#copy
#print(dic.copy())

#fromkeys() Le asigna el numero a todas las llaves
#print(dic.fromkeys(['a','b','c'],3))

#GET Recibe como parametro del diccionario y devuelve un parametro como valor
#print(dic.get('name'))

#POP recibe un parametro por ID lo elimina y deviulve el valor
#print(dic.pop('lastname'))
print(dic)

#SET
"""car = {"brand": "Ford",  "model": "Mustang",  "year": 1964}
x = car.setdefault("model", "Bronco")
print(x)"""

#Update
"""car = {"brand": "Ford",  "model": "Mustang",  "year": 1964}
car.update({"color": "White"})
print(car)"""

#################### Ejercicio de Diccionario ########################

