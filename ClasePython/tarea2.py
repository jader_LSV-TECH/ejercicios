# Variables
personajes={}
list=[]
detener='s'
SalMin=0
sbasic=int(850000)
horaOrd=8
cont=0
# While que indica el ingreso de los Valores y hasta cuando se debe hacer
while detener == 's':

    cont += 1
    print("################ Empleado N°", cont, "######################")

    nom = input("Nombre: ")
    sm = int(input("Salario Minimo: "))
    hed=int(input("Hora extra Diurno: "))
    hn=int(input("horario normal: "))
    hen=int(input("horas extras nocturno: "))
    hedf=int(input("horas extras diurno.f: "))
    henf=int(input("horas extras nocturno.f: "))
    dias=int(input("Dias Trabajados: "))

    if (personajes.get(nom) == None):               #Compara si no hay datos en Personajes

        SalTot=sbasic*sm                            # Salario Minimo
        HED = (horaOrd*1.25)                        # Horas Extras Diurna
        cesantias = ((sm * dias)/360)               # Sesantias
        isc = (cesantias * dias * 0.12/360)         # Intereses Sobre Sesantias
        ps = (sm * (dias/6)/360)                    # Prima de Servicios
        vaca = ((sm * sbasic * dias) / 720)         # Vacaciones
        HN = (horaOrd * 1.35)                       # Hora Nocturna
        HEN = (horaOrd * 1.75)                      # Hora Extra Nocturna
        HODF = (horaOrd * 1.75)                     # Hora Ordinaria Diurna Festiva
        HEDDF = (horaOrd * 2)                       # Hora extra diurna dominical o festiva
        HENDF = (horaOrd * 2.5)                     # Hora extra nocturna dominical o festivo

        LQD=cesantias*ps*vaca                       # Liquidaciones
        TP=LQD+SalTot                               # TOTAL PAGO

        #TGE=TGE+TP                                  # Total de Gastos pagados por la empresa

        personajes.update({                         #Ingresa Datos mediante el diccionario
            'dias': dias,
            'Nombre':nom,
            'SM':[sm,SalTot],
            'HED':[hed,HED],
            'hn':[hn,HN],
            'hen':[hen,HEN],
            'hendf':[hedf,HEDDF],
            'henf':[henf,HENDF],
            'Liquidacion':[LQD],
            'Total Pago':[TP]
        })
        list.append(personajes)                     #Introduce Datos a la lista mediante el diccionario
        detener = input("¿Desea Agregar a otro (s/n)?")
    else:
        detener = input("Contacto Existe Desea remplazarlo (s/n)")
print(list)

"""for item in list:
    print(item)"""